from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from tasks import views


urlpatterns = [
    path('', views.index),
    path('tasks/<int:id>/', views.task_view, name='task'),
    path('tasks/<int:id>/edit/', views.edit_task, name='edit_task'),
    path('tasks/<int:id>/delete/', views.delete_task, name='delete_task'),
    path('tasks/new/', views.new_task, name='new_task'),

    path('login/', auth_views.LoginView.as_view()),
    path('logout/', auth_views.LogoutView.as_view()),
    
    path('register/', views.MyRegisterFormView.as_view()),
    path('profile/', views.ProfilePage.as_view()),
    path('edit_profile/', views.edit_profile),

    path('admin/', admin.site.urls),
    path('select2/', include('django_select2.urls')),
] 
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
