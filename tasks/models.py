from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save 


class Status(models.Model):
    title = models.CharField(max_length=225)
    position = models.PositiveIntegerField()
    
    class Meta:
        verbose_name_plural = 'Statuses'
        ordering = ['position', 'id']
    
    def __str__(self):
        return self.title

class Task(models.Model):
    title = models.CharField(max_length=225)
    body = models.TextField()
    status = models.ForeignKey(
        Status,
        blank=True, null=True,
        on_delete=models.CASCADE,
    )

    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE)
    responsive_users = models.ManyToManyField(
    	'auth.User', related_name='responsive_for',
    	blank=True,
    	)

    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']


    def __str__(self):
        return self.title

    @property
    def body_as_html(self):
        import markdown
        return markdown.markdown(self.body)
    
class Comment(models.Model):
    body = models.TextField()
    task = models.ForeignKey(
        Task, related_name='comments',
        on_delete=models.CASCADE)
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.body[:30]

    @property
    def body_as_html(self):
        import markdown
        return markdown.markdown(self.body)

class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	first_name = models.CharField(max_length=250)
	last_name = models.CharField(max_length=250)
	email = models.EmailField(max_length=250)
	image = models.ImageField(upload_to='profile_image', blank=True)

	def __str__(self):
		return self.user.username

def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

# post_save.connect(create_profile, sender=User)