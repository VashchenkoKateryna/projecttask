from django.shortcuts import redirect


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        path = request.path_info
        if not request.user.is_authenticated and path != '/register/':
            
            if path != '/login/' and not path.startswith('/admin/'):
                return redirect('/login/?next=%s' % path)

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response