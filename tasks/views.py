from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden, Http404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .models import Task, Status
from .forms import TaskForm, CommentForm
from django.contrib.auth.views import PasswordResetView
from django.contrib.auth.views import PasswordChangeView


def index(request):
    tasks = Task.objects.all()
    statuses = Status.objects.all()
    

    f_search = request.GET.get('search', '')
    f_status = request.GET.get('status')
    f_my = request.GET.get('my')


    selected_status = None

    if f_search:
        tasks = tasks.filter(title__icontains=f_search)
    if f_status and f_status != 'all':
        selected_status = int(f_status)
        tasks = tasks.filter(status_id=f_status)
    if f_my == 'on':
        tasks = tasks.filter(author=request.user)

    paginator = Paginator(tasks, 5)
    page = request.GET.get('page')
    tasks = paginator.get_page(page)

    context = {
    'tasks': tasks,
    'statuses': statuses,
    'paginator': paginator,
    'f_search': f_search,
    'f_status': f_status,
    'f_my': f_my,
    'page': page,
    'selected_status': selected_status,
    }
    return render(request, 'index.html', context)


def task_view(request, id):
    task = get_object_or_404(Task, id=id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.task = task
            comment.save()
            return redirect(reverse('task', args=[task.id]))
    else:
        form = CommentForm()
    return render(request, 'task.html', { 
    	'task': task, 'comment_form': form})


def new_task(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid:
            task = form.save(commit=False)
            task.author = request.user
            task.save()
            return redirect('/')
    else:
        form = TaskForm()
    return render(request, 'new_task.html', {'form': form})


def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    if request.user != task.author:
        return HttpResponseForbidden('Access denied')

    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid:
            form.save()
            return redirect(reverse('task', args=[task.id]))
    else:
        form = TaskForm(instance=task)
    return render(request, 'edit_task.html', {'form': form})

def delete_task(request, id):
    task = get_object_or_404(Task, id=id)

    if request.user != task.author:
        return HttpResponseForbidden('Access denied')

    if request.method == 'POST':
        task.delete()
        return redirect('/')
    else:
        return render(request, 'delete_task.html', {'task': task})



from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm


from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.views.generic.base import TemplateView

class MyForm(UserCreationForm):
    email = forms.EmailField(max_length=200, help_text='Required')
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class MyRegisterFormView(FormView):
    form_class = MyForm
    success_url = "/login/"
    template_name = "registration/register.html"

    def form_valid(self, form):
        form.save()
        return super(MyRegisterFormView, self).form_valid(form)
    def form_invalid(self, form):
        return super(MyRegisterFormView, self).form_invalid(form)

class ProfilePage(TemplateView):
    template_name = "registration/profile.html"


from .forms import EditProfile

def edit_profile(request):
    # args = {}

    if request.method == 'POST':
        form = EditProfile(request.POST, instance=request.user)
        if form.is_valid():
            # if 'image' in request.FILES:
            #     form.image = request.FILES['image']
            form.save()
            # return HttpResponseRedirect(reverse('update_profile_success'))
            return render(request, 'registration/profile.html')
    else:
        form = EditProfile(instance=request.user)

    # args['form'] = form
    return render(request, 'registration/edit_profile.html',  {'form': form})


