from django.contrib import admin
from .models import Status, Task, Comment

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ('title', 'position')

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 0

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'status', 'added')
    list_filter = ('status',)
    search_fields = ('title', 'body')
    date_hierarchy = 'added'
    filter_horizontal = ('responsive_users',)
    inlines = [CommentInline]

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('task', 'author', 'short_text', 'added')
    search_fields = ('title', 'body')
    date_hierarchy = 'added'
    raw_id_fields = ('task', 'author')

    def short_text(self, obj):
        return obj.body[:50]