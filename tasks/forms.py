from django import forms
from django_select2.forms import Select2MultipleWidget
from django.contrib.auth.models import User
from .models import Task, Comment


class TaskForm(forms.ModelForm):
    responsive_users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        widget=Select2MultipleWidget,
        )
    class Meta:
        model = Task
        fields = ('title', 'body', 'status', 'responsive_users')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)

class EditProfile(forms.ModelForm):
    username = forms.CharField(required=True)
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    email = forms.EmailField(required=True)
    # image = forms.ImageField(required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)

    def clean_email(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError('This email address is already in use. Please supply a different email address.')
        return email

    # def save(self, commit=True):
    #     user = super(RegistrationForm, self).save(commit=False)
    #     user.email = self.cleaned_data['email']

    #     if commit:
    #         user.save()
    #     return user

